
# s3 bucket for our state file
resource "aws_s3_bucket" "state_bucket_1" {
  bucket = "sunday_project_state_remotestate"
}

resource "aws_s3_bucket_acl" "state_bucket_acl" {
  bucket = aws_s3_bucket.state_bucket_1.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "state_bucket_versioning" {
  bucket = aws_s3_bucket.state_bucket_1.id
  versioning_configuration {
    status = "Enabled"
  }
}
# dynamoDB table for our backend
resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "terraform_state_lock"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }


}
# aws instance
resource "aws_instance" "sunday_instance" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"

  tags = {
    Name = "sunday_instance"
  }
}